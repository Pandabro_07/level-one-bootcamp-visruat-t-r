//WAP to find the volume of a tromboloid using 4 functions.

#include <stdio.h>

float input()
{
    float f;
    printf("enter a dimension for the tromboloid\n");//function 1
    scanf("%f",&f);
    return f;
}

float vol_trom(float a,float c,float e)//function 2
{
    float res=0.0;
    res=(((a*e)+e)*1/3)/c;
    return res;
}

void output(float g,float i,float k,float l)//function 3
{
    printf("The volume of the tromboloid with dimensions %.2f , %.2f and %.2f is %.2f \n",g,i,k,l);
}

int main()//function 4
{
    float h=input();
    float b=input();
    float d=input();
    float vol=vol_trom(h,b,d);
    output(h,b,d,vol);
    return 0;
}


