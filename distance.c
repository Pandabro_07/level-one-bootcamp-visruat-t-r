//WAP to find the distance between two point using 4 functions.

#include <stdio.h>
#include <math.h>

int n=5;

float input()
{
    float b;
    --n;
    while(n>0)
    {
        if (n==4)
        {
            printf("enter the absicca for the first point\n");
            scanf("%f",&b);
            return b;
        }
        if(n==3)
        {
            printf("enter the ordinate for the first point\n");
            scanf("%f",&b);
            return b;
        }
        if(n==2)
        {
            printf("enter the absicca for the second point\n");
            scanf("%f",&b);
            return b;
        }
        if(n==1)
        {
            printf("enter the ordinate for the second point\n");
            scanf("%f",&b);
            return b;
        }
    }
}


float dist(float x1,float y1,float x2,float y2)//function 2
{
    float res=0;
    res=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
    return res;
}

void output(float x1,float y1,float x2,float y2,float distance)//function 3
{
    printf("the distance between point(%.1f,%.1f) amd (%.1f,%.1f) is %.2f\n",x1,y1,x2,y2,distance);
}

int main()//function 4
{
    float x1,y1,x2,y2,distance=0.0;
    x1=input();
    y1=input();
    x2=input();
    y2=input();
    distance=dist(x1,y1,x2,y2);
    output(x1,y1,x2,y2,distance);
    return 0;
}