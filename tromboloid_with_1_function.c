//Write a program to find the volume of a tromboloid using one function

#include <stdio.h>

int main()//function 1
{
    float h,b,d,vol=0.0;// variable declaration

    printf("Enter the height, breadth and depth \nin the form: \nnn.nn nn.nn nn.nn!\n");
    scanf("%f %f %f",&h,&b,&d);//reading input

    vol=(((h*d)+d)*1/3)/b;//(calculating volume)

    printf("The volume of the tromboloid with dimensions %.2f , %.2f and %.2f is %.2f ",h,b,d,vol);//printing output
    return 0;
}