//Write a program to add two user input numbers using 4 functions.

#include <stdio.h>

float input()
{
    float a;
    printf("enter a number\n");//function 1
    scanf("%f",&a);
    return a;
}

float add(float b,float c)//function 2
{
    float res=0;
    res=b+c;
    return res;
}

void output(float d,float e,float f)//function 3
{
    printf("the sum of %.2f and %.2f is %.2f \n",d,e,f);
}

int main()//function 4
{
    float g=input();// function call
    float h=input();
    float sum=add(g,h);
    output(g,h,sum);
    return 0;
}


