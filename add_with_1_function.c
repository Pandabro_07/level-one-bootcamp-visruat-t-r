//Write a program to add two user input numbers using one function.

#include <stdio.h>

int main()//function 1
{
    float a,b,sum=0;// variable declaration

    printf("Enter the two numbers\nin the form: \nnn.nn nn.nn !\n");
    scanf("%f %f",&a,&b);//reading input

    sum=a+b;//(addition)

    printf("The sum of %.2f and %.2f is %.2f ",a,b,sum);//printing output
    return 0;
}
