//WAP to find the distance between two points using structures and 4 functions.

#include <stdio.h>
#include <math.h>

struct point
    {
        float x;
        float y;
    };
typedef struct point Point;
Point input()
    {
        Point p;
        printf("enter the absicca\n");
        scanf("%f",&p.x);
        printf("enter the ordinate\n");
        scanf("%f",&p.y);
        return p;
    }

float compute(Point p1,Point p2)
    {
        float res=sqrt(pow((p2.x-p1.x),2)+pow((p2.y-p1.y),2));
        return res;
    }

void output(Point p1,Point p2,float distance)
    {
        printf("The distance between (%.1f,%.1f) and (%.1f,%.1f) is %.2f \n",p1.x,p1.y,p2.x,p2.y,distance);
    }

int main()
    {
        float dist;
        Point p1,p2;
        p1=input();
        p2=input();
        dist=compute(p1,p2);
        output(p1,p2,dist);
        return 0;
    }
